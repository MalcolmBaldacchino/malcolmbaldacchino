﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ColourChanger : MonoBehaviour {

	public int Counter = 0;

	public Color Colour0;
	public Color Colour1;
	public Color Colour2;
	public Color Colour3;
	public Color Colour4;

	static SpriteRenderer Background = null;

	void Awake () {
		if (Background == null) {
			Background = GameObject.Find ("Background").GetComponent<SpriteRenderer> ();
			GameObject.DontDestroyOnLoad (this.gameObject);
		} else {
			Destroy (this.gameObject);
		}
	}

	void OnEnable() {
		SceneManager.sceneLoaded += OnSceneLoaded;
	}

	void OnDisable() {
		SceneManager.sceneLoaded -= OnSceneLoaded;
	}

	private void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
		Button ColourButton = GameObject.Find ("BackgroundButton").GetComponent<Button> ();
		ColourButton.onClick.AddListener (ChangeColour);
	}
	
	void ChangeColour () {
		if (Counter == 0) {
			Background.color = Colour1;
			Counter ++;
		}
		else if (Counter == 1) {
			Background.color = Colour2;
			Counter ++;
		}
		else if (Counter == 2) {
			Background.color = Colour3;
			Counter ++;
		}
		else if (Counter == 3) {
			Background.color = Colour4;
			Counter ++;
		}
		else if (Counter == 4) {
			Background.color = Colour0;
			Counter = 0;
		}
	}
}