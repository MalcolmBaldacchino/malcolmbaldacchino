﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MusicPlayer : MonoBehaviour {

	static MusicPlayer myMusicPlayer = null;

	public bool isMute = false;

	void Awake () {
		if (myMusicPlayer == null) {
			//myMusicPlayer = 1st Sound
			myMusicPlayer = this;
			GameObject.DontDestroyOnLoad (this.gameObject);
		} else {
			Destroy (this.gameObject);
		}
	}

	void OnEnable() {
		SceneManager.sceneLoaded += OnSceneLoaded;
	}

	void OnDisable() {
		SceneManager.sceneLoaded -= OnSceneLoaded;
	}

	private void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
		Button MuteButton = GameObject.Find ("MuteButton").GetComponent<Button> ();
		MuteButton.onClick.AddListener (Mute);
	}

	public void Mute () {
		if (isMute == false) {
			AudioListener.volume = 0;
			isMute = true;
		} else {
			AudioListener.volume = 1;
			isMute = false;
		}
	}
}
