﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {

	public void LoadNextLevel(){
		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex + 1);
	}

	public void LoadLevel(string levelName){
		print ("Level loaded: " + levelName);
		SceneManager.LoadScene (levelName);
	}
		
	public void ExitGame(){
		//to quit when game is tested in unity
		//UnityEditor.EditorApplication.isPlaying = false;
		Application.Quit();
	}

}