﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameTracker : MonoBehaviour {

	public static int Church = 50;
	public static int People = 50;
	public static int Military = 50;
	public static int Treasury = 50;

	static GameObject Tracker = null;

	void Awake () {
		if (Tracker == null) {
			Tracker = GameObject.Find ("GameTracker").GetComponent<GameObject> ();
			GameObject.DontDestroyOnLoad (this.gameObject);
		} else {
			Destroy (this.gameObject);
		}
	}

	void OnEnable() {
		SceneManager.sceneLoaded += OnSceneLoaded;
	}

	void OnDisable() {
		SceneManager.sceneLoaded -= OnSceneLoaded;
	}

	private void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
		if (SceneManager.GetActiveScene().buildIndex < 15 && SceneManager.GetActiveScene().buildIndex > 1){
			Text Pillar_Church = GameObject.Find ("Church").GetComponent<Text> ();
			Pillar_Church.text = "Church" + "\n" + Church;
			Text Pillar_People = GameObject.Find ("People").GetComponent<Text> ();
			Pillar_People.text = "People" + "\n" + People;
			Text Pillar_Military = GameObject.Find ("Military").GetComponent<Text> ();
			Pillar_Military.text = "Military" + "\n" + Military;
			Text Pillar_Treasury = GameObject.Find ("Treasury").GetComponent<Text> ();
			Pillar_Treasury.text = "Treasury" + "\n" + Treasury;
		}

		if ((Church < 99 && Church > 1) && (People < 99 && People > 1) && (Military < 99 && Military > 1) && (Treasury < 99 && Treasury > 1)) {
			
			//Diplomat Scene
			if (SceneManager.GetActiveScene ().name == "Diplomat") {
				Button LeftButton = GameObject.Find ("LeftButton").GetComponent<Button> ();
				LeftButton.onClick.AddListener (LeftDip);
				Button RightButton = GameObject.Find ("RightButton").GetComponent<Button> ();
				RightButton.onClick.AddListener (RightDip);
			}

			//Priest Scene
			else if (SceneManager.GetActiveScene ().name == "Priest") {
				Button LeftButton = GameObject.Find ("LeftButton").GetComponent<Button> ();
				LeftButton.onClick.AddListener (LeftPri);
				Button RightButton = GameObject.Find ("RightButton").GetComponent<Button> ();
				RightButton.onClick.AddListener (RightPri);
			}

			//Princess Scene
			else if (SceneManager.GetActiveScene ().name == "Princess") {
				Button LeftButton = GameObject.Find ("LeftButton").GetComponent<Button> ();
				LeftButton.onClick.AddListener (LeftCes);
				Button RightButton = GameObject.Find ("RightButton").GetComponent<Button> ();
				RightButton.onClick.AddListener (RightCes);
			}

			//Bishop Scene
			else if (SceneManager.GetActiveScene ().name == "Bishop") {
				Button LeftButton = GameObject.Find ("LeftButton").GetComponent<Button> ();
				LeftButton.onClick.AddListener (LeftBis);
				Button RightButton = GameObject.Find ("RightButton").GetComponent<Button> ();
				RightButton.onClick.AddListener (RightBis);
			}

			//Queen Scene
			else if (SceneManager.GetActiveScene ().name == "Queen") {
				Button LeftButton = GameObject.Find ("LeftButton").GetComponent<Button> ();
				LeftButton.onClick.AddListener (LeftQue);
				Button RightButton = GameObject.Find ("RightButton").GetComponent<Button> ();
				RightButton.onClick.AddListener (RightQue);
			}

			//Nobleman Scene
			else if (SceneManager.GetActiveScene ().name == "Nobleman") {
				Button LeftButton = GameObject.Find ("LeftButton").GetComponent<Button> ();
				LeftButton.onClick.AddListener (LeftNob);
				Button RightButton = GameObject.Find ("RightButton").GetComponent<Button> ();
				RightButton.onClick.AddListener (RightNob);
			}

			//Farmer Scene
			else if (SceneManager.GetActiveScene ().name == "Farmer") {
				Button LeftButton = GameObject.Find ("LeftButton").GetComponent<Button> ();
				LeftButton.onClick.AddListener (LeftFar);
				Button RightButton = GameObject.Find ("RightButton").GetComponent<Button> ();
				RightButton.onClick.AddListener (RightFar);
			}

			//WhiteBird Scene
			else if (SceneManager.GetActiveScene ().name == "WhiteBird") {
				Button LeftButton = GameObject.Find ("LeftButton").GetComponent<Button> ();
				LeftButton.onClick.AddListener (LeftWhi);
				Button RightButton = GameObject.Find ("RightButton").GetComponent<Button> ();
				RightButton.onClick.AddListener (RightWhi);
			}

			//Duke Scene
			else if (SceneManager.GetActiveScene ().name == "Duke") {
				Button LeftButton = GameObject.Find ("LeftButton").GetComponent<Button> ();
				LeftButton.onClick.AddListener (LeftDuk);
				Button RightButton = GameObject.Find ("RightButton").GetComponent<Button> ();
				RightButton.onClick.AddListener (RightDuk);
			}

			//General Scene
			else if (SceneManager.GetActiveScene ().name == "General") {
				Button LeftButton = GameObject.Find ("LeftButton").GetComponent<Button> ();
				LeftButton.onClick.AddListener (LeftGen);
				Button RightButton = GameObject.Find ("RightButton").GetComponent<Button> ();
				RightButton.onClick.AddListener (RightGen);
			}

			//Merchant Scene
			else if (SceneManager.GetActiveScene ().name == "Merchant") {
				Button LeftButton = GameObject.Find ("LeftButton").GetComponent<Button> ();
				LeftButton.onClick.AddListener (LeftMer);
				Button RightButton = GameObject.Find ("RightButton").GetComponent<Button> ();
				RightButton.onClick.AddListener (RightMer);
			}

			//Executioner Scene
			else if (SceneManager.GetActiveScene ().name == "Executioner") {
				Button LeftButton = GameObject.Find ("LeftButton").GetComponent<Button> ();
				LeftButton.onClick.AddListener (LeftExe);
				Button RightButton = GameObject.Find ("RightButton").GetComponent<Button> ();
				RightButton.onClick.AddListener (RightExe);
			}

			//Jester Scene
			else if (SceneManager.GetActiveScene ().name == "Jester") {
				Button LeftButton = GameObject.Find ("LeftButton").GetComponent<Button> ();
				LeftButton.onClick.AddListener (LeftJes);
				Button RightButton = GameObject.Find ("RightButton").GetComponent<Button> ();
				RightButton.onClick.AddListener (RightJes);
			} else {
				Church = 50;
				People = 50;
				Military = 50;
				Treasury = 50;
				Destroy (this.gameObject);
			}
		} else {
			SceneManager.LoadScene ("Lose");
			Church = 50;
			People = 50;
			Military = 50;
			Treasury = 50;
			Destroy (this.gameObject);
		}
	}

	//Diplomat
	void RightDip (){
		Church = Church + 10;
		People = People + 10;
		Military = Military + 10;
		Treasury = Treasury - 25;
	}
	void LeftDip () {
		Church = Church - 10;
		People = People - 10;
		Military = Military - 10;
	}

	//Priest
	void RightPri (){
		Church = Church - 10;
		People = People + 10;
		Military = Military - 10;
		Treasury = Treasury - 10;
	}
	void LeftPri () {
		Church = Church - 10;
		People = People - 25;
		Military = Military - 10;
	}

	//Princess
	void RightCes (){
		People = People + 10;
		Military = Military + 10;
		Treasury = Treasury - 10;
	}
	void LeftCes () {
		People = People - 10;
		Military = Military - 10;
	}

	//Bishop
	void RightBis (){
		Military = Military - 10;
	}
	void LeftBis () {
		People = People - 10;
	}

	//Queen
	void RightQue (){
		Church = Church - 10;
		People = People - 25;
		Military = Military + 10;
	}
	void LeftQue () {
		People = People + 10;
		Military = Military - 10;
	}

	//Nobleman
	void RightNob (){
		Church = Church + 10;
		Military = Military + 10;
	}
	void LeftNob () {
		People = People + 10;
		Treasury = Treasury + 10;
	}

	//Farmer
	void RightFar (){
		Church = Church + 10;
		People = People + 10;
		Military = Military + 25;
	}
	void LeftFar () {
		Church = Church + 10;
		People = People + 10;
		Military = Military + 10;
		Treasury = Treasury + 10;
	}

	//Whitebird
	void RightWhi (){
		Military = Military - 10;
		Treasury = Treasury - 10;
	}
	void LeftWhi () {
		People = People - 10;
		Treasury = Treasury - 10;
	}

	//Duke
	void RightDuk (){
		Treasury = Treasury - 25;
	}
	void LeftDuk () {
		Treasury = Treasury - 10;
	}

	//General
	void RightGen (){
		People = People - 10;
		Military = Military + 25;
		Treasury = Treasury - 25;
	}
	void LeftGen () {
		Military = Military - 25;
	}

	//Merchant
	void RightMer (){
		Treasury = Treasury + 10;
	}
	void LeftMer () {
		Church = Church + 10;
		People = People + 10;
	}

	//Executioner
	void RightExe (){
		Church = Church - 25;
		People = People + 10;
		Military = Military - 25;
	}
	void LeftExe () {
		Military = Military + 10;
	}

	//Jester
	void RightJes (){
		People = People + 10;
	}
	void LeftJes () {
		Church = Church + 10;
	}
}